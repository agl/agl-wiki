Wiki collating all the info on AGL.

https://docs.automotivelinux.org/en/octopus/# is the main AGL maintained wiki
and this wiki here is a abridged version as this wiki is too vast and it helps
you sync and build for renesas R3 board

[[_TOC_]]
# sync

```
	# bash alias to sync and copy the drivers
	sagl() {
		cd /home/sashok/AGL
		mkdir $1
		cp r3-drivers/R-Car_Gen3_Series_Evaluation_Software_Package_* $1/
		cd $1
		repo init -u https://gerrit.automotivelinux.org/gerrit/AGL/AGL-repo
		repo sync
	}
```
the drivers is in the 2nd step are absolutely needed these drivers build will
not start they are downloaded from
[URL](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-h3-m3-documents-software)

# Build
* I use fedora and AGL cannot be built there as it is not one of those
  environments on which AGL builds are verified. the build system will through
  the following error.
```
NOTE: Your conf/bblayers.conf has been automatically updated.
WARNING: Host distribution "fedora-36" has not been validated with this version of the build system; you may possibly experience unexpected failures. It is recommended that you use a tested distribution.
```
* AGL is better built in a container(in which the build is verified) than in the
native Linux environment. Here is a [docker based container
repository](https://gitlab.collabora.com/sashok/agl-builder) pls follow the
instructions to make a Docker based container, log into it and build.
## bitbake tricks

>bitbake -e \<myrecipe\>

Shows the global or per-package environment complete with information about where variables were set/changed.
>devtool modify \<myrecipe\>

Fetches and places the remote code in local filesystem and there after local changes can be tried here.
>devtool reset \<myrecipe\>

Once you are done with the debugging, either updated the recipe or created a patch, *reset* severs that link and the code will be fetched from remote afresh

# Documenation
Here is the official wireplumber/pipewire [documenation](https://docs.automotivelinux.org/en/master/#5_Component_Documentation/8_pipewire_wireplumber/)
## Contributing to documentation

```
git clone https://git.automotivelinux.org/AGL/documentation

Edit docs/5_Component_Documentation/8_pipewire_wireplumber.md
git add
git commit -s -m"...................."
git review -s
git review
```

# contributing to AGL

```
git remote add am ssh://achidipothu@gerrit.automotivelinux.org:29418/apps/agl-service-audiomixer
git push am HEAD:refs/for/octopus #am here is the remote added above.

git remote add meta-agl-demo ssh://achidipothu@gerrit.automotivelinux.org:29418/AGL/meta-agl-demo
git push meta-agl-demo HEAD:refs/for/octopus #meta-agl-demo here is the remote added above.

git remote add meta-agl ssh://achidipothu@gerrit.automotivelinux.org:29418/AGL/meta-agl
git push meta-agl HEAD:refs/for/octopus #meta-agl here is the remote branch
added bove
git push meta-agl HEAD:refs/for/master #meta-agl here is the ssh URL above I believe.

git push agl HEAD:refs/for/lamprey #agl here is the ssh URL above I believe.
```

Indiviual SSH URL can be obtained from https://gerrit.automotivelinux.org/gerrit/admin/repos/AGL/meta-agl

# Hardwares

# Different types of AGL Boards

[List of AGL Boards is here](https://docs.automotivelinux.org/en/octopus/#01_Getting_Started/02_Building_AGL_Image/09_Building_for_Supported_Renesas_Boards/#14-making-sure-your-build-environment-is-correct)

# Using AGL target

[AGL wiki to use the renesas
board](https://docs.automotivelinux.org/en/octopus/#01_Getting_Started/02_Building_AGL_Image/09_Building_for_Supported_Renesas_Boards/)
if you use a different board, you will have to change the board type.

## Serial Interface
- picocom -b 115200 /dev/ttyUSB0 #cmd to launch serial interface over USB.
- this interface can be used to login and get the debug msgs.

## Ethernet Interface
- The board comes with a ethernet port and you connect a ethernet cable if you have one
  into it and you can ssh into it.
	Use the Serial Interface to know the ipaddress of the board.
```
root@h3ulcb:~# ifconfig
eth0      Link encap:Ethernet  HWaddr 00:80:45:1B:40:7C
          inet addr:192.168.29.45  Bcast:192.168.29.255  Mask:255.255.255.0
          inet6 addr: 2405:201:c413:30a3:280:45ff:fe1b:407c/64 Scope:Global
          inet6 addr: fe80::280:45ff:fe1b:407c/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:58 errors:0 dropped:8 overruns:0 frame:0
          TX packets:76 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:6552 (6.3 KiB)  TX bytes:10160 (9.9 KiB)
          Interrupt:129

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:931 errors:0 dropped:0 overruns:0 frame:0
          TX packets:931 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:131042 (127.9 KiB)  TX bytes:131042 (127.9 KiB)


```
- `rsync` can be use to copy files to the machine as well as copy a few files.
for example I use the following alias to login as well as copy aliases files and
a few music files to the target.
for testing.
```
export AGL_IP=192.168.0.160
sshh() {
    echo "rsync ~/.bashrc root@"$AGL_IP":~"
    rsync ~/Dropbox/config/.bashrc root@"$AGL_IP":~/.bashrc
    rsync ~/Music/test1.wav root@"$AGL_IP":
    rsync ~/Music/stest.wav root@"$AGL_IP":
    ssh root@"$AGL_IP"
}
```
rsync can result in the following error.
```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
SHA256:wiLTtO53Y5LHt8U0czdNtlQLayAo/f9HYw9UYopxGbI.
Please contact your system administrator.
Add correct host key in /home/sashok/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/sashok/.ssh/known_hosts:17
Host key for 192.168.0.160 has changed and you have requested strict checking.
Host key verification failed.

```
this can be fixed with the below command.

```
export AGL_IP=192.168.0.160
alias rkey='ssh-keygen -R $AGL_IP'
```

`scp` cannot be used as it is deprecated.



- Here is a [video recording](how-to-use-agl-ref-hardware.MOV) of how to use the AGL reference h/w.
  - download and play it if you are unable to play it in the browser.
- Hardware machine name is h3ulcb

> source meta-agl/scripts/aglsetup.sh -f -m h3ulcb -b build-h3ulcb agl-demo agl-devel agl-localdev agl-refhw-h3

# Audio

## AGL Usecases & Testcases

[agl-testcases-usecases.md](agl-testcases-usecases.md)

## Notes in pipewire upgrade
* First update the pipewire and wireplumber versions in the recipe files and try to bitbake them, you will most likely run into compilation issues
* compilation issues:
  * they are mostly due to lower meson version used in AGL.
  * Download the version of meson that is used in AGL and try to compile the pipewire and wireplumber with it in host system.
  * There is no need for clean fixes here, just revert some patches and see if the compilation goes through.
  * prepare patches and update the reciepies for them.
  * devtool of bitbake is also useful at this juncture to try local fixes.
* After the compilation is through then verify the funtionality.
* Funtional Issues:
  * They are mostly due to the lua config files, first up check if they are any differences by manually comparing the files with their upstream counter parts.
  * Make changes if you think some thing should be picked up.



## Test reports

* [SPEC 4418(Fix HDMI on Rpi) test report](pipewire_upgrade_1.0.0_spec_4418(rpi).ods)
* [pipewire 1.0.0 upgrade test report](pipewire_upgrade_1.0.0.ods)
* [pipewire 0.3.40 upgrade test report](pipewire_upgrade_0.3.40_test_report.ods)
* [pipewire 0.3.43 upgrade test report](pipewire_upgrade_0.3.43_test_report.ods)
* [pipewire 0.3.47 upgrade test report](pipewire_upgrade_0.3.47_test_report.ods)
* [T32540__pipewire_lxc_config](T32540__pipewire_lxc_config.ods)
* [pipewire 0.3.68 upgrade test report](pipewire_upgrade_0.3.68_test_report.ods)

# changes
* [pipewire 0.3.68 upgrade](https://gerrit.automotivelinux.org/gerrit/q/topic:pipewireupgrade-0.3.67)
  * plus https://gerrit.automotivelinux.org/gerrit/q/topic:pipewireupgrade-0.3.67
# LXC

## Documenation
https://docs.automotivelinux.org/en/master/#5_Component_Documentation/9_ic-sound-manager/


## build cmds
```
source meta-agl/scripts/aglsetup.sh -f -m $MACHINE -b build-h3ulcb agl-demo agl-devel agl-localdev agl-refhw-h3
time bitbake lxc-host-image-demo

```
[the JIRA](https://jira.automotivelinux.org/projects/SPEC/issues/SPEC-4155?filter=doneissues)

## History
gerrit where the wireplumber instances are split up - https://gerrit.automotivelinux.org/gerrit/c/AGL/meta-agl/+/26662

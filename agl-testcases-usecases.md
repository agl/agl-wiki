[[_TOC_]]

# Hardware needed
- Speaker and mic
  - AGL hardware has seperate 3.5 MM ports for MIC and Speaker like the old laptops use to have and so you may needed a [Jack splitter](https://www.amazon.co.uk/MillSO-3-5mm-Jack-Headphone-Adapter-1T2-20/dp/B0727NBHBV/ref=sr_1_4?crid=3I1Q62VVRRXJC&keywords=microphone+headset+jack+adapter&qid=1637837828&qsid=260-2421526-4592564&sprefix=microhp%2Caps%2C421&sr=8-4&sres=B073ZCWDZY%2CB0727NBHBV%2CB09GNK3BJJ%2CB073ZDDTH2%2CB099QYZQNT%2CB01MTY00M0%2CB08JS9N98L%2CB004SP0WAQ%2CB099QYN6GL%2CB07K47JRJ9%2CB08LBJT48T%2CB08DFDBQXK%2CB08QSLMSGT%2CB01M0T6PSF%2CB08QDJF7JQ%2CB073QMD8T1&srpt=ELECTRONIC_ADAPTER)
- BT wireless adaptor or controller.
  - AGL hardware's BT controller cannot be used at the moment and so [BT adaptar](https://www.amazon.in/gp/product/B07NQ5YGDW/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) will be needed.

# Playback

* With GStreamer: 
>gst-launch-1.0 audiotestsrc ! pipewiresink stream-properties="p,media.role=Multimedia"
* With GStreamer via ALSA: 
>gst-launch-1.0 audiotestsrc ! alsasink device="pipewire:ROLE=Multimedia"
* With pw-play: 
>pw-play --media-role Multimedia stest.wav
* With aplay: 
>aplay -D pipewire:ROLE=Multimedia stest.wav
# Navigation App Audio: 
 - plug a HDMI monitor to HDMI of the ref hardware.
 - plug a mouse as well
 - on the screen goto nav app and start a ride
 - audio should be heard in stereo.


# Capture

* With GStreamer: 
>gst-launch-1.0 pipewiresrc ! wavenc ! filesink location=test.wav
* With GStreamer via ALSA:
>gst-launch-1.0 alsasrc device=pipewire ! wavenc ! filesink location=test.wav
* With pw-record: 
```
pw-record test.wav #this cmd records with a default media role of Music.
pw-record --media-role Voice test.wav
```
* With arecord: 
>arecord -D pipewire test.wav

# Concurrency Scenarios

## MM + MM

* Play some music with the `Multimedia` role
>pw-play --media-role Multimedia test1.wav
* In parallel, play some other music also with the `Multimedia` role
>pw-play --media-role Multimedia stest.wav
* Expected result: when the second music starts, the first one should stop; when the second music stops, the first one should resume (cork behavior)

## MM + Navigation

* Play some music with the `Multimedia` role
>pw-play --media-role Multimedia test1.wav
* In parallel, play some speech with the `Navigation` role
> pw-play --media-role Navigation stest.wav
* Expected result: music should be audible with a lower volume while the navigation message is playing and then it should be restored to its original volume (duck behavior)

## MM + Speech

* Play some music with the `Multimedia` role
>pw-play --media-role Multimedia test1.wav
* In parallel, play some speech with the `Speech-Low` role
>pw-play --media-role Speech-Low stest.wav
* Expected result: when the speech starts, music sthould stop; when the speech stops, music should resume (cork behavior)

## MM + Record

* Record something with your microphone
* Play it back... it should be exactly as recorded
* Repeat recording while some music is also playing... playback should not affect the recording in any way
```
arecord -D pipewire test.wav 
or
pw-record --media-role Voice test.wav
```

# bluetooth
## HFP

* Ensure the AGL machine has both speakers and microphone connected
* Link your smartphone to the AGL machine
* Make a call on the phone, calling a second phone that you own (could be VoIP, landline, doesn't matter)
* The speakers and mic of the AGL machine should function as a headset for the phone; talking to the mic should route audio to the speaker of the second phone & talking to the second phone should route audio to the speakers of the AGL machine.

## How to link your smart phone via Bluetooth to AGL Hardware.
- got to bluetoothctl prompt in the Hardware.
```
root@h3ulcb:~# bluetoothctl
Agent registered
```
- get the list of bt controllers in the system and select the right one.
```
[bluetooth]# list
Controller 78:04:73:F9:8D:CB h3ulcb #1 [default]
Controller 00:1A:7D:DA:71:15 h3ulcb 
[bluetooth]# select 00:1A:7D:DA:71:15
Controller 00:1A:7D:DA:71:15 h3ulcb [default]
```
- pick the mac address of your controller(check [Hardware needed](#hardware-needed)), how do I know which one is my controller? plug the wireless bt controller and pick the newest addition in the `bluetoothctl list` 

- find the MAC address of your mobile phone
```
[bluetooth]# scan on
Discovery started
[CHG] Controller 00:1A:7D:DA:71:15 Discovering: yes
[NEW] Device 4F:06:52:36:9B:4A 4F-06-52-36-9B-4A
[CHG] Device 4F:06:52:36:9B:4A RSSI: -52
[CHG] Device 4F:06:52:36:9B:4A RSSI: -43
[CHG] Device 4F:06:52:36:9B:4A RSSI: -35
[NEW] Device 08:95:42:24:FA:5E ASHOK’s iPhone
[CHG] Device 4F:06:52:36:9B:4A RSSI: -47
[CHG] Device 4F:06:52:36:9B:4A RSSI: -33
[CHG] Device 4F:06:52:36:9B:4A RSSI: -48
``` 
- pair with mobile phone
* First key in the pairing request as shown below in the terminal and then confirm the key value in the mobile phone. `do not intiate` the request from mobile phone side.
```
[bluetooth]# pair 08:95:42:24:FA:5E
Attempting to pair with 08:95:42:24:FA:5E
[CHG] Device 08:95:42:24:FA:5E Connected: yes
Request confirmation
[agent] Confirm passkey 725006 (yes/no): yes
[CHG] Device 08:95:42:24:FA:5E ServicesResolved: yes
[CHG] Device 08:95:42:24:FA:5E Paired: yes
Pairing successful
```
- connect and trust the MAC address

```
[ASHOK’s iPhone]# connect 08:95:42:24:FA:5E
Attempting to connect to 08:95:42:24:FA:5E
[CHG] Device 4F:06:52:36:9B:4A RSSI: -38
Connection successful
```
- to know the list of bluetooth devices(not controllers) connected
```
[bluetooth]# devices
Device 79:99:4C:89:60:95 79-99-4C-89-60-95
Device 08:95:42:24:FA:5E ASHOK’s iPhone
```
## A2DP playback
Use the above same procedure to Link with your phone and play some music on the phone(youtube, spotify, music app) etc. The audio should be heard in the headset of the ref hardware.

## what to do for the below error?
```
[bluetooth]# scan on
Failed to start discovery: org.bluez.Error.NotReady
```
Running "rfkill unblock all" helped here this is from the following [page](https://unix.stackexchange.com/questions/508221/bluetooth-service-running-but-bluetoothctl-says-org-bluez-error-notready).



# Volume Mixer
This feature is only supported in lamprey branch.
* Play music in MM role and try to slide the volume bars. the slides that should show effect are ONLY "master playback" & "playback:Multimedia"
* Play music in Navigation role and try to slide the volume bars. the slides that should show effect are ONLY  "master playback" & "playback:Navigation"
* Play music in Speech-Low role and try to slide the volume bars. the slides that should show effect are ONLY  "master playback" & "playback:Speech-Low"
